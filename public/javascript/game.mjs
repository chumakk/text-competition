import { addClass } from "./helpers/helper.mjs";
import { handleUsernameExists, handleDisconnect } from "./socketHandlers/userHandler.mjs";
import {
  handleRemoveRoom,
  handleRoomExists,
  handleActiveRooms,
  handleAddRoom,
  handleJoinInUser,
  handleJoinIn,
  handleRoomNotExist,
  handleRoomIsFull,
  handleRoomIsPlaying,
  handleLeavedRoom,
  handleSuccessLeaved,
  handleResetRoom,
} from "./socketHandlers/roomHandler.mjs";
import {
  handleToggleStatusReady,
  handlePrepareGame,
  handleStartCountdown,
  handleStartGame,
  handleGameOver,
  handleGameCountDown,
  handleSetProgress,
} from "./socketHandlers/gameHandler.mjs";
import { readyUserBtn, leaveRoomBtn, gameUsersTable, gameModalWrapper } from "./global.mjs";

const createRoomBtn = document.querySelector("#add-room-btn");
const username = sessionStorage.getItem("username");
const closeGameModalBtn = document.querySelector("#quit-results-btn");

if (!username) {
  window.location.replace("/login");
}

const socket = io("", { query: { username } });

socket.on("USERNAME_EXISTS", handleUsernameExists);

socket.on("ACTIVE_ROOMS", (rooms) => handleActiveRooms(socket, rooms));

socket.on("ROOM_EXISTS", handleRoomExists);

createRoomBtn.addEventListener("click", () => {
  const name = prompt("Please enter the name");
  if (name) {
    socket.emit("CREATE_ROOM", name);
    return;
  }
  alert("Invalid name");
});

leaveRoomBtn.addEventListener("click", () => {
  socket.emit("LEAVE_ROOM");
});

readyUserBtn.addEventListener("click", () => {
  socket.emit("SET_TOGGLE_STATUS_READY");
});

closeGameModalBtn.addEventListener("click", (e) => {
  gameUsersTable.innerHTML = "";
  addClass(gameModalWrapper, "display-none");
});

socket.on("ADD_ROOM", (room) => handleAddRoom(socket, room));

socket.on("JOINED_IN_USER", (room) => handleJoinInUser(socket, room));

socket.on("JOINED_IN", (room) => handleJoinIn(socket, room));

socket.on("ROOM_NOT_EXIST", (name) => handleRoomNotExist(socket, name));

socket.on("ROOM_IS_FULL", (name) => handleRoomIsFull(socket, name));

socket.on("REMOVE_ROOM", (name) => handleRemoveRoom(socket, name));

socket.on("ROOM_IS_PLAYING", (name) => handleRoomIsPlaying(socket, name));

socket.on("LEAVED_ROOM", (room) => handleLeavedRoom(socket, room));

socket.on("SUCCESS_LEAVED", handleSuccessLeaved);

socket.on("SETETTED_TOGGLE_STATUS_READY", (user) => handleToggleStatusReady(socket, user));

socket.on("PREPARE_GAME", handlePrepareGame);

socket.on("START_COUNTDOWN", handleStartCountdown);

socket.on("START_GAME", (seconds) => handleStartGame(socket, seconds));

socket.on("GAME_OVER", (users) => handleGameOver(socket, users));

socket.on("GAME_COUNTDOWN", handleGameCountDown);

socket.on("SET_PROGRESS", (user) => handleSetProgress(user));

socket.on("RESET_ROOM", (room) => handleResetRoom(socket, room));

socket.on("disconnect", handleDisconnect);
