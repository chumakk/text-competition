const roomContainer = document.querySelector(".room-container");
const roomPage = document.querySelector("#rooms-page");
const gamePage = document.querySelector("#game-page");
const gameUsersContainer = document.querySelector(".room-users");
const readyUserBtn = document.querySelector("#ready-btn");
const leaveRoomBtn = document.querySelector("#quit-room-btn");
const timer = document.querySelector("#timer");
const gameTimer = document.querySelector("#gameTimer");
const gameText = document.querySelector("#text-container");
const gameModalWrapper = document.querySelector(".game-modal-wrapper");
const gameUsersTable = document.querySelector(".game-users-table");
const roomNameContainer = document.querySelector(".room-name");

export {
  roomContainer,
  roomPage,
  gamePage,
  gameUsersContainer,
  readyUserBtn,
  leaveRoomBtn,
  timer,
  gameTimer,
  gameText,
  gameUsersTable,
  gameModalWrapper,
  roomNameContainer
};
