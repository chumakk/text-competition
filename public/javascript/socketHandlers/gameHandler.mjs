import { addClass, removeClass } from "../helpers/helper.mjs";
import {
  readyUserBtn,
  leaveRoomBtn,
  timer,
  gameText,
  gameTimer,
  gameUsersTable,
  gameModalWrapper,
} from "../global.mjs";
import { updateUserStatus, createUserRezultElement } from "../views/user.mjs";
import { setTextGame, onInputTextWrapper } from "../listeners/game.mjs";

const handleToggleStatusReady = (socket, user) => {
  updateUserStatus(user);
  if (socket.id === user.id) {
    readyUserBtn.innerHTML = user.isReady ? "NOT READY" : "READY";
  }
};

const handlePrepareGame = (index) => {
  addClass(readyUserBtn, "display-none");
  addClass(leaveRoomBtn, "not-visible");
  removeClass(timer, "display-none");
  fetch(`/game/texts/${index}`, {
    method: "GET",
  })
    .then((res) => res.text())
    .then((text) => setTextGame(text));
};

const handleStartCountdown = (time) => {
  timer.innerHTML = time;
};

const handleStartGame = (socket, seconds) => {
  addClass(timer, "display-none");
  timer.innerHTML = "";
  removeClass(gameText, "display-none");
  removeClass(gameTimer, "display-none");
  gameTimer.innerHTML = `${seconds} seconds left`;

  document.addEventListener("keydown", onInputTextWrapper(socket));
};

const sortNumbers = (user, nextUser) => {
  if (!user.finishTime || !nextUser.finishTime) {
    if (!user.finishTime && !nextUser.finishTime) return 0;
    if (!user.finishTime) return 1;
    if (!nextUser.finishTime) return -1;
  }
  return user.finishTime - nextUser.finishTime;
};

const handleGameOver = (socket, users) => {
  document.removeEventListener("keydown", onInputTextWrapper(socket));
  addClass(gameText, "display-none");
  addClass(gameTimer, "display-none");
  gameText.innerHTML = "";
  gameTimer.innerHTML = "";
  const sortedUsers = [...users].sort(sortNumbers);
  const usersElements = sortedUsers.map((user, index) => createUserRezultElement(index + 1, user));
  gameUsersTable.append(...usersElements);
  removeClass(gameModalWrapper, "display-none");
};

const handleGameCountDown = (seconds) => {
  gameTimer.innerHTML = `${seconds} seconds left`;
};

const handleSetProgress = (user) => {
  const progressBar = document.querySelector(`.user-progress.${user.name}`);
  if (user.progress === 100) addClass(progressBar, "finished");
  progressBar.style.width = `${user.progress}%`;
};

export {
  handleToggleStatusReady,
  handlePrepareGame,
  handleStartCountdown,
  handleStartGame,
  handleGameOver,
  handleGameCountDown,
  handleSetProgress,
};
