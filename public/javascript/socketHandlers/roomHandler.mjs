import {
  roomContainer,
  roomPage,
  gamePage,
  gameUsersContainer,
  readyUserBtn,
  leaveRoomBtn,
  roomNameContainer,
} from "../global.mjs";
import { addClass, removeClass } from "../helpers/helper.mjs";
import { createRoomElement, updateConnectedUsers, deleteRoomElement } from "../views/room.mjs";
import { createUserElement } from "../views/user.mjs";

const handleRoomExists = (name) => {
  alert(`The room with name ${name} exists`);
};

const handleActiveRooms = (socket, rooms) => {
  rooms.forEach((room) => {
    roomContainer.append(createRoomElement({ ...room, socket }));
  });
};

const handleAddRoom = (socket, room) => {
  roomContainer.append(createRoomElement({ ...room, socket }));
};

const handleJoinInUser = (socket, room) => {
  updateConnectedUsers(room);
  if (!room.users.some((user) => user.id === socket.id)) return;
  gameUsersContainer.innerHTML = "";
  room.users.forEach((user) => {
    gameUsersContainer.append(createUserElement({ ...user, socket }));
  });
};

const handleJoinIn = (socket, room) => {
  addClass(roomPage, "display-none");
  roomNameContainer.innerHTML = `${room.name}`;
  gameUsersContainer.innerHTML = "";
  readyUserBtn.innerHTML = "Ready";
  room.users.forEach((user) => {
    gameUsersContainer.append(createUserElement({ ...user, socket }));
  });
  removeClass(gamePage, "display-none");
};

const handleRemoveRoom = (socket, name) => {
  deleteRoomElement(socket, name);
};

const handleLeavedRoom = (socket, room) => {
  updateConnectedUsers(room);
  if (!room.users.some((user) => user.id === socket.id)) return;
  gameUsersContainer.innerHTML = "";
  room.users.forEach((user) => {
    gameUsersContainer.append(createUserElement({ ...user, socket }));
  });
};

const handleSuccessLeaved = () => {
  addClass(gamePage, "display-none");
  roomNameContainer.innerHTML = "";
  gameUsersContainer.innerHTML = "";
  removeClass(roomPage, "display-none");
};

const handleResetRoom = (socket, room) => {
  gameUsersContainer.innerHTML = "";
  const elements = room.users.map((user) => createUserElement({ ...user, socket }));
  gameUsersContainer.append(...elements);
  removeClass(readyUserBtn, "display-none");
  readyUserBtn.innerHTML = "READY";
  removeClass(leaveRoomBtn, "not-visible");
};

const handleRoomNotExist = (socket, name) => {
  deleteRoomElement(socket, name);
  alert("Room not exist");
};

const handleRoomIsFull = (socket, name) => {
  deleteRoomElement(socket, name);
  alert("Room is full");
};

const handleRoomIsPlaying = (socket, name) => {
  deleteRoomElement(socket, name);
  alert("Room is playing now!");
};

export {
  handleRoomExists,
  handleActiveRooms,
  handleAddRoom,
  handleJoinInUser,
  handleJoinIn,
  handleRemoveRoom,
  handleLeavedRoom,
  handleSuccessLeaved,
  handleResetRoom,
  handleRoomNotExist,
  handleRoomIsFull,
  handleRoomIsPlaying,
};
