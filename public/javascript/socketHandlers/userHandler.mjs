import { addClass, removeClass } from "../helpers/helper.mjs";
import {
  roomContainer,
  roomPage,
  gamePage,
  gameUsersContainer,
  readyUserBtn,
  leaveRoomBtn,
  timer,
  gameTimer,
  gameText,
  gameUsersTable,
  gameModalWrapper,
  roomNameContainer,
} from "../global.mjs";

const handleUsernameExists = (username) => {
  alert(`The user with username ${username} exists`);
  sessionStorage.removeItem("username");
  window.location.replace("/login");
};

const handleDisconnect = () => {
  addClass(gamePage, "display-none");
  addClass(gameModalWrapper, "display-none");
  addClass(timer, "display-none");
  addClass(gameTimer, "display-none");
  addClass(gameText, "display-none");
  removeClass(roomPage, "display-none");
  removeClass(readyUserBtn, "display-none");
  removeClass(leaveRoomBtn, "not-visible");

  roomNameContainer.innerHTML = "";
  gameUsersContainer.innerHTML = "";
  roomContainer.innerHTML = "";
  gameUsersTable.innerHTML = "";
  readyUserBtn.innerHTML = "READY";
  timer.innerHTML = "";
  gameTimer.innerHTML = "";
  gameText.innerHTML = "";
};

export { handleUsernameExists, handleDisconnect };
