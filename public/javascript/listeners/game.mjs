import { gameText } from "../global.mjs";

let textGame = "";
let allSymbols = [];
let inputedSymbols = [];
let nextSymbol;

const setTextGame = (text) => {
  textGame = text;
};

const createInputText = (allSymbols, inputedSymbols, nextSymbol) => {
  const inputed = `<span class="inputed-text">${inputedSymbols.join("")}</span>`;
  const next = `<span class="next-symbol">${nextSymbol}</span>`;
  const other = allSymbols.slice(inputedSymbols.length + 1).join("");
  return inputed + next + other;
};

const onInputText = (socket, e) => {
  if (e.repeat) return;
  if (e.key === nextSymbol && inputedSymbols.length !== allSymbols.length) {
    inputedSymbols.push(e.key);
    nextSymbol = inputedSymbols.length === allSymbols.length ? "" : allSymbols[inputedSymbols.length];
    gameText.innerHTML = createInputText(allSymbols, inputedSymbols, nextSymbol);
    const progress = (inputedSymbols.length / allSymbols.length) * 100;
    socket.emit("SET_PROGRESS", progress);
  }
};

const onInputTextWrapper = (socket) => {
  allSymbols = textGame.split("");
  inputedSymbols = [];
  nextSymbol = allSymbols[0];
  gameText.innerHTML = createInputText(allSymbols, inputedSymbols, nextSymbol);
  return (e) => onInputText(socket, e);
};

export { setTextGame, onInputTextWrapper };
