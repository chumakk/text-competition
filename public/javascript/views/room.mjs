import { createElement } from "../helpers/helper.mjs";

const handleJoinRoom = (socket) => {
  return (e) => {
    const roomName = e.target.getAttribute("data-room-name");
    socket.emit("JOIN_ROOM", roomName);
  };
};

const createRoomElement = ({ name, users, socket }) => {
  const roomElement = createElement({ tagName: "div", className: "room", attributes: { "data-room-name": name } });
  const activeUsersElement = createElement({ tagName: "div", className: "active-users" });
  activeUsersElement.append(`${users.length} users connected`);

  const roomNameElement = createElement({ tagName: "div", className: "room-name" });
  roomNameElement.append(`${name}`);

  const roomJoinElement = createElement({
    tagName: "button",
    className: "join-btn",
    attributes: { "data-room-name": name },
  });
  roomJoinElement.append("Join");
  roomJoinElement.addEventListener("click", handleJoinRoom(socket));

  roomElement.append(activeUsersElement, roomNameElement, roomJoinElement);
  return roomElement;
};

const updateConnectedUsers = ({ name, users }) => {
  const activeUsersElement = document.querySelector(`.room[data-room-name='${name}'] .active-users`);
  if (activeUsersElement) {
    activeUsersElement.innerHTML = `${users.length} users connected`;
  }
};

const deleteRoomElement = (socket, name) => {
  const roomElement = document.querySelector(`.room[data-room-name='${name}']`);
  if (roomElement) {
    const roomJoinElement = document.querySelector(`.join-btn[data-room-name='${name}']`);
    roomJoinElement.removeEventListener("click", handleJoinRoom(socket));
    roomElement.remove();
  }
};

export { createRoomElement, updateConnectedUsers, deleteRoomElement };
