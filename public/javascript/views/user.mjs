import { createElement, addClass, removeClass } from "../helpers/helper.mjs";

const createUserElement = ({ name, id, isReady, progress, socket }) => {
  const userElement = createElement({ tagName: "div", className: "user" });
  const userInfoElement = createElement({ tagName: "div", className: "user-info flex" });

  const userReadyElement = createElement({ tagName: "div", className: `ready-status ${name}` });
  if (isReady) {
    addClass(userReadyElement, "ready-status-green ");
  } else {
    addClass(userReadyElement, "ready-status-red");
  }
  const usernameElement = createElement({ tagName: "div", className: "username" });
  usernameElement.append(`${name}`);
  if (socket.id === id) {
    addClass(usernameElement, "active");
  }
  userInfoElement.append(userReadyElement, usernameElement);
  const userProgressIndicatorElement = createElement({ tagName: "div", className: "user-progress-indicator" });
  const userProgress = createElement({
    tagName: "div",
    className: `user-progress ${name}`,
    attributes: { styles: `width: ${progress}%;` },
  });
  userProgressIndicatorElement.append(userProgress);

  userElement.append(userInfoElement, userProgressIndicatorElement);
  return userElement;
};

const updateUserStatus = (user) => {
  const userStatusElement = document.querySelector(`.ready-status.${user.name}`);
  if (user.isReady) {
    removeClass(userStatusElement, "ready-status-red");
    addClass(userStatusElement, "ready-status-green");
  } else {
    removeClass(userStatusElement, "ready-status-green");
    addClass(userStatusElement, "ready-status-red");
  }
};

const createUserRezultElement = (position, user) => {
  const userElement = createElement({
    tagName: "div",
    className: "user-position",
    attributes: { id: `place-${position}` },
  });
  userElement.append(`${position}. ${user.name}`);

  return userElement
};

export { createUserElement, updateUserStatus, createUserRezultElement };