export const activeUsers = new Map();
export const activeRooms = new Map();
export const playingRooms = new Set();

export const getUserById = (id) => {
  for (const user of activeUsers) {
    if (user[1] === id) {
      return { name: user[0], id: user[1] };
    }
  }
};
