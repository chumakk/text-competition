import { handleJoinRoom, handleCreateRoom, handleDisconnecting, handleLeaveRoom } from "./handlers/roomHandler";
import { getActiveRooms, handleCreateUser } from "./handlers/userHandler";
import { handleToggleStatusReady, handleSetProgress } from "./handlers/gameHandler";

export default (io) => {
  io.on("connection", (socket) => {
    if (!handleCreateUser(socket)) {
      return;
    }
    getActiveRooms(socket);

    socket.on("CREATE_ROOM", (name) => handleCreateRoom(io, socket, name));

    socket.on("JOIN_ROOM", (roomName) => handleJoinRoom(io, socket, roomName));

    socket.on("disconnecting", () => handleDisconnecting(io, socket));

    socket.on("LEAVE_ROOM", () => handleLeaveRoom(io, socket));

    socket.on("SET_TOGGLE_STATUS_READY", () => handleToggleStatusReady(io, socket));

    socket.on("SET_PROGRESS", (progress) => handleSetProgress(io, socket, progress));
  });
};
