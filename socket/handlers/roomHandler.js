import { MAXIMUM_USERS_FOR_ONE_ROOM } from "../config";
import { activeUsers, activeRooms, playingRooms, getUserById } from "../store";
import { handleStartGame, readyForGame } from "./gameHandler";

const handleJoinRoom = (io, socket, roomName) => {
  const room = activeRooms.get(roomName);

  if (!room) {
    socket.emit("ROOM_NOT_EXIST", roomName);
    return;
  }

  if (room.users.length === MAXIMUM_USERS_FOR_ONE_ROOM) {
    socket.emit("ROOM_IS_FULL", roomName);
    return;
  }

  if (playingRooms.has(roomName)) {
    socket.emit("ROOM_IS_PLAYING", roomName);
    return;
  }

  activeRooms.set(roomName, {
    ...room,
    users: [...room.users, { ...getUserById(socket.client.id), isReady: false, progress: 0, finishTime: null }],
  });

  socket.join(roomName);

  const joinedRoom = activeRooms.get(roomName);
  if (joinedRoom.users.length === MAXIMUM_USERS_FOR_ONE_ROOM) {
    io.emit("REMOVE_ROOM", joinedRoom.name);
  }

  socket.broadcast.emit("JOINED_IN_USER", joinedRoom);
  socket.emit("JOINED_IN", joinedRoom);
};

const handleCreateRoom = (io, socket, name) => {
  if (activeRooms.has(name)) {
    socket.emit("ROOM_EXISTS", name);
    return;
  }
  const room = {
    name,
    users: [{ ...getUserById(socket.client.id), isReady: false, progress: 0, finishTime: null }],
  };
  activeRooms.set(room.name, room);
  socket.join(room.name);
  io.emit("ADD_ROOM", activeRooms.get(room.name));
  socket.emit("JOINED_IN", activeRooms.get(room.name));
};

function maybeStartGameOrAddRoom(io, room, updatedRoom) {
  if (playingRooms.has(updatedRoom.name)) return;

  if (readyForGame(updatedRoom)) {
    handleStartGame(io, updatedRoom);
  } else {
    if (room.users.length === MAXIMUM_USERS_FOR_ONE_ROOM) {
      io.emit("ADD_ROOM", activeRooms.get(updatedRoom.name));
    }
  }
}

const handleLeaveRoom = (io, socket) => {
  const outgoingUser = getUserById(socket.client.id);
  const rooms = Object.keys(socket.rooms);
  rooms.forEach((nameRoom) => {
    if (activeRooms.has(nameRoom)) {
      const room = activeRooms.get(nameRoom);
      activeRooms.set(nameRoom, { ...room, users: room.users.filter((user) => user.id !== outgoingUser.id) });
      const updatedRoom = activeRooms.get(nameRoom);
      if (updatedRoom.users.length === 0) {
        activeRooms.delete(nameRoom);
        playingRooms.delete(room.name);
        io.emit("REMOVE_ROOM", nameRoom);
      } else {
        maybeStartGameOrAddRoom(io, room, updatedRoom);
        io.emit("LEAVED_ROOM", updatedRoom);
      }
      socket.leave(nameRoom);
      socket.emit("SUCCESS_LEAVED");
    }
  });
};

const handleDisconnecting = (io, socket) => {
  const outgoingUser = getUserById(socket.client.id);
  activeUsers.delete(outgoingUser.name);
  const rooms = Object.keys(socket.rooms);
  rooms.forEach((nameRoom) => {
    if (activeRooms.has(nameRoom)) {
      const room = activeRooms.get(nameRoom);
      activeRooms.set(nameRoom, { ...room, users: room.users.filter((user) => user.id !== outgoingUser.id) });
      const updatedRoom = activeRooms.get(nameRoom);
      if (updatedRoom.users.length === 0) {
        activeRooms.delete(nameRoom);
        playingRooms.delete(room.name);
        socket.broadcast.emit("REMOVE_ROOM", nameRoom);
      } else {
        maybeStartGameOrAddRoom(io, room, updatedRoom);
        socket.broadcast.emit("LEAVED_ROOM", updatedRoom);
      }
    }
  });
};

export { handleJoinRoom, handleCreateRoom, handleDisconnecting, handleLeaveRoom };
