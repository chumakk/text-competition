import { activeRooms, playingRooms, getUserById } from "../store";
import { texts } from "../../data";
import { SECONDS_TIMER_BEFORE_START_GAME, SECONDS_FOR_GAME, MAXIMUM_USERS_FOR_ONE_ROOM } from "../config";

const readyForGame = (room) => {
  return room.users.every((user) => {
    return user.isReady === true;
  });
};

const handleToggleStatusReady = (io, socket) => {
  const owner = getUserById(socket.client.id);
  const rooms = Object.keys(socket.rooms);
  rooms.forEach((nameRoom) => {
    if (activeRooms.has(nameRoom)) {
      const room = activeRooms.get(nameRoom);
      activeRooms.set(nameRoom, {
        ...room,
        users: room.users.map((user) => (user.id !== owner.id ? user : { ...user, isReady: !user.isReady })),
      });
      io.to(nameRoom).emit(
        "SETETTED_TOGGLE_STATUS_READY",
        activeRooms.get(nameRoom).users.find((user) => user.id === owner.id)
      );

      if (readyForGame(activeRooms.get(nameRoom))) {
        handleStartGame(io, room);
      }
    }
  });
};

const handleSetProgress = (io, socket, progress) => {
  const rooms = Object.keys(socket.rooms);
  const currentRoomName = rooms.find((room) => activeRooms.has(room));
  if (!currentRoomName) return;
  const currentRoom = activeRooms.get(currentRoomName);
  activeRooms.set(currentRoomName, {
    ...currentRoom,
    users: currentRoom.users.map((user) => {
      if (user.id === socket.client.id) {
        if (progress === 100) {
          user.finishTime = Date.now();
        }
        user.progress = progress;
      }
      return user;
    }),
  });

  const currentStateRoom = activeRooms.get(currentRoomName);

  if (currentStateRoom.users.every((user) => user.progress === 100)) {
    gameOver(io, currentStateRoom);
    return;
  }

  const updatedUser = currentStateRoom.users.find((user) => user.id === socket.client.id);
  io.to(currentRoomName).emit("SET_PROGRESS", updatedUser);
};

const getRandomIndex = (mas) => {
  return Math.floor(Math.random() * mas.length);
};

const gameOver = (io, currentStateRoom, interval) => {
  io.to(currentStateRoom.name).emit("GAME_OVER", currentStateRoom.users);
  activeRooms.set(currentStateRoom.name, {
    ...currentStateRoom,
    users: currentStateRoom.users.map((user) => ({ ...user, isReady: false, progress: 0, finishTime: null })),
  });
  io.to(currentStateRoom.name).emit("RESET_ROOM", activeRooms.get(currentStateRoom.name));
  if (interval) {
    clearInterval(interval);
  }

  playingRooms.delete(currentStateRoom.name);

  const resetStateRoom = activeRooms.get(currentStateRoom.name);
  if (resetStateRoom && resetStateRoom.users.length !== MAXIMUM_USERS_FOR_ONE_ROOM) {
    io.emit("ADD_ROOM", resetStateRoom);
  }
};

const handleStartGame = async (io, room) => {
  io.emit("REMOVE_ROOM", room.name);
  io.to(room.name).emit("PREPARE_GAME", getRandomIndex(texts));
  playingRooms.add(room.name);
  await new Promise((resolve, reject) => {
    const startTime = Date.now();
    io.to(room.name).emit("START_COUNTDOWN", SECONDS_TIMER_BEFORE_START_GAME);
    const interval = setInterval(() => {
      if (!activeRooms.get(room.name)) {
        clearInterval(interval);
        resolve();
        return;
      }
      const currentTime = Date.now();
      const difference = Math.round((currentTime - startTime) / 1000);
      if (difference < SECONDS_TIMER_BEFORE_START_GAME) {
        const countdown = SECONDS_TIMER_BEFORE_START_GAME - difference;
        io.to(room.name).emit("START_COUNTDOWN", countdown);
      } else {
        io.to(room.name).emit("START_COUNTDOWN", 0);
        clearInterval(interval);
        resolve();
      }
    }, 1000);
  });

  io.to(room.name).emit("START_GAME", SECONDS_FOR_GAME);
  const startTime = Date.now();
  const interval = setInterval(() => {
    const currentStateRoom = activeRooms.get(room.name);
    if (!currentStateRoom || !playingRooms.has(room.name)) {
      clearInterval(interval);
      return;
    }

    if (currentStateRoom.users.every((user) => user.progress === 100)) {
      gameOver(io, currentStateRoom, interval);
      return;
    }
    const currentTime = Date.now();
    const difference = Math.round((currentTime - startTime) / 1000);
    if (difference < SECONDS_FOR_GAME) {
      const countdown = SECONDS_FOR_GAME - difference;
      io.to(room.name).emit("GAME_COUNTDOWN", countdown);
    } else {
      gameOver(io, currentStateRoom, interval);
    }
  }, 1000);
};

export { handleToggleStatusReady, handleStartGame, handleSetProgress, readyForGame };
