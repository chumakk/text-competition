import { MAXIMUM_USERS_FOR_ONE_ROOM }  from "../config";
import { activeUsers, activeRooms, playingRooms } from "../store";

const handleCreateUser = (socket) => {
    const username = socket.handshake.query.username;
    if (activeUsers.has(username)) {
      socket.emit("USERNAME_EXISTS", username);
      return false;
    }
    activeUsers.set(username, socket.client.id);
    return true;
}

const getActiveRooms = (socket) =>{
    const rooms = []
    activeRooms.forEach((room) => {
        if(room.users.length !== MAXIMUM_USERS_FOR_ONE_ROOM && !playingRooms.has(room.name)){
            rooms.push(room)
        }
    });
    socket.emit("ACTIVE_ROOMS", rooms)
}

export { handleCreateUser, getActiveRooms }