export const texts = [
  "I just returned from the greatest summer vacation!",
  "We visited many famous tourist places.",
  "George is at the pet store, looking at what kind of pet he might want to get for his birthday.",
  "George sees the animals in tanks.",
  "Keith recently came back from a trip to Chicago.",
  "In June, Diane visited her friends who live in San Francisco, California.",
  "On the first day of her trip, Diane visited the Golden Gate Bridge.",
  "London is a famous and historic city. It is the capital of England in the United Kingdom.",
];

export default { texts };
